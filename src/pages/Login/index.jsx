import React from 'react';
import LayoutComponent from '../../components/Layout';
import FormLogin from '../../components/FormLogin/'
import Image from '../../components/FormLogin/Image.jsx'

const Login = () => {
    return (
        <LayoutComponent
            leftColSize={{ xs: 24, sm: 24, md: 4, lg: 6 }}
            rigthColSize={{ xs: 0, sm: 0, md: 20, lg: 18 }}
            leftContent={<Image />}
            rightContent={<FormLogin />}
        />
    );
}

export default Login;