import { Form, Input, Button, Card } from 'antd';
import React, { useState, useContext } from 'react';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import './FormLogin.css';
import { useNavigate, Link } from 'react-router-dom';
import authService from '../../services/auth';
import { AuthContext } from '../../context/AuthContext';

const FormLogin = () => {
    const navigate = useNavigate();
    const [loginError, setLoginError] = useState(false);
    const [loading, setLoading] = useState(false);

    const { login } = useContext(AuthContext);

    const onFinish = async (values) => {
        setLoading(true);
        setLoginError(false);
        try {
            const response = await authService.loginF(values.username, values.password);
            if (response && response.data) {
                localStorage.setItem('token', response.data.token);
                login(response.data.token);
                navigate('/'); // Redirect the user to the dashboard or another page
            } else {
                console.log('Error en el inicio de sesión: Respuesta inesperada');
                setLoginError(true);
            }
        } catch (error) {
            console.error('Error en el inicio de sesión', error.response ? error.response.data : error.message);
            setLoginError(true);
        } finally {
            setLoading(false);
        }
        console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <>
            <Card title="Bienvenido de nuevo!" bordered={false} className='responsive-card'>
                <Form
                    name='normal_login'
                    className='login-form'
                    initialValues={{
                        remember: true,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Form.Item
                        name="username"
                        rules={[
                            {
                                required: true,
                                message: 'Favor de ingresar su usuario'
                            }
                        ]}
                    >
                        <Input prefix={<UserOutlined />} placeholder='Correo' />
                    </Form.Item>
                    <Form.Item
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: 'Por favor ingrese su contraseña'
                            }
                        ]}
                    >
                        <Input.Password prefix={<LockOutlined />} placeholder='Contraseña' />
                    </Form.Item>
                    <Form.Item>
                        {loginError && <p style={{ color: 'red' }}>Falló el inicio de sesión</p>}
                        <Button type="primary" htmlType='submit' className='login-form-button' loading={loading}>
                            Iniciar sesión
                        </Button>
                    </Form.Item>
                    ¿Aún no tienes cuenta? <Link to='/registro'>Regístrate</Link>
                </Form>
            </Card>
        </>
    );
}

export default FormLogin;
