export const ENV = {
  API_URL: "http://localhost:3000",
  ENDPOINTS: {
    LOGIN: 'api/auth/signin',
    REGISTER: "api/auth/signup",
    USER: "api/user",
    PRODUCTS: "api/products"
  },
  STORAGE: {
    Token: "token",
  }
}