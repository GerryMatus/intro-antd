import { Form, Input, Button, Card } from 'antd';
import React, { useState } from 'react';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import './FormRegister.css'
import { useNavigate } from 'react-router-dom';
import authService from '../../services/auth';
import { validatePassword } from '../../utils/validation.js';



const FormRegister = () => {

    const navigate = useNavigate();

    const [registerError, setRegisterError] = useState(false);

    const [loading, setLoading] = useState(false);

    const onFinish = async (values) => {
        setLoading(true);

        try {
            await authService.register(values.username, values.email, values.password);
            console.log("Registro exitoso");
            navigate('/login');
        } catch (error) {
            console.error('Error en el registro', error.response.data);
            setRegisterError(true);
        } finally {
            setLoading(false);
        }

        console.log('Success:', values);
    }
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    }

    /* const validatePassword = ({getFieldValue}) => ({
        validator(_, value) {
            if( !value || getFieldValue('password') === value ) {
                return Promise.resolve();
            }
            return Promise.reject(new Error('Las contraseñas no coinciden'));
        }
    }) */

    return (
        <>
            <Card title="Registro de usuario" bordered={false} classNames='responsive-card'>

                <Form
                    name='normal_register'
                    className='register-form'
                    initialValues={{
                        remember: true,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}

                >
                    <Form.Item
                        name="username"
                        rules={[
                            {
                                required: true,
                                message: 'Favor de ingresar su usuario'
                            }
                        ]}
                    >
                        <Input prefix={<UserOutlined className="site-form-item-icon"/> } classname="username" placeholder='Usuario' />
                    </Form.Item>
                    
                    <Form.Item
                        name="email"
                        rules={[
                            {
                                required: true,
                                message: 'Por favor ingrese su email',
                            }
                        ]}
                    >
                        <Input prefix={<UserOutlined className='site-form-item-icon'/>} placeholder='Correo' />
                    </Form.Item>

                    <Form.Item
                        name='password'
                        rules={[
                            {
                                required: true,
                                message: "Por favor ingrese su contraseña",
                            }
                        ]}
                    >
                        <Input.Password 
                        prefix={<LockOutlined className='site-form-item-icon'/>}
                        type='password'
                        placeholder='Contraseña' />
                    </Form.Item>
                    
                    <Form.Item
                    name='password-repet'
                    rules={[
                        {
                            required: true,
                            message: 'Por favor repita su contraseña'
                        },
                        ( { getFieldValue } ) => validatePassword( {getFieldValue} ),
                    ]}
                    >
                       <Input.Password 
                        prefix={<LockOutlined className='site-form-item-icon'/>}
                        type='password'
                        placeholder='Confirmar contraseña' /> 
                    </Form.Item>

                    <Form.Item>
                        {registerError && <p style={{color: 'red'}}>Falló el registro</p>}
                        <Button type="primary" className='login-form-button' loading={loading} htmlType='submit'>
                            Registrarse
                        </Button>
                    </Form.Item>
                    ¿Ya tienes una cuenta? <a href='./login'>Iniciar Sesión</a>
                </Form>
            </Card>
        </>
    );
}

export default FormRegister;