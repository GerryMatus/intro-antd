import { ENV } from "../utils/constants";

//función para almacenar el token en el local storage
const setToken = (token) => {
    localStorage.setItem(ENV.STORAGE.Token, token);
}

const getToken = () => {
    return localStorage.getItem(ENV.STORAGE.Token);
}

const removeToken = () => {
    localStorage.removeItem(ENV.STORAGE.Token);
}

export const storageController = {
    setToken,
    getToken,
    removeToken,
}