import React from 'react';
import { Button } from 'antd';
import { PlusOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons';

const CrudButtons = () => {
    return (
        <div style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center', gap: '10px' }}>
            <Button type="primary" icon={<PlusOutlined />}></Button>
            <Button type="default" icon={<EditOutlined />}></Button>
            <Button type="primary" danger icon={<DeleteOutlined />}></Button>
        </div>
    );
}

export default CrudButtons;
