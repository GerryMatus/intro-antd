import { Layout, Menu } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';
const { Headers } = Layout;
import logo from "../../assets/yo.png";
import './Nav.css';
import { Header } from 'antd/es/layout/layout';
import DrawerComponent from '../Drawer/';

const Nav = () => {

    const tabNames = ["", "Productos", "Servicios", "Contacto"];
    const items = tabNames.map((name, index) => ({
        key: index + 1,
        label: name,
        url: index === 0 ? "/" : `/${name.toLowerCase()}`,
    }));

    return (
        <>
            <Layout>
                <Header className='header-content'>
                    <Link to='/'>
                        <img src={logo} height='75' width='75' alt='logo' />
                    </Link>
                    <Menu
                        theme='black'
                        mode='horizontal'
                        defaultSelectedKeys={['1']}
                        style={{
                            display: 'flex',
                            justifyContent: 'flex-end',
                            flex: 1,
                            minWidth: 0,
                            marginRight: '20px'
                        }}
                    >
                        {items.map(item => (
                            <Menu.Item key={item.key}>
                                <Link to={item.url}>{item.label}</Link>
                            </Menu.Item>
                        ))}
                    </Menu>
                    <DrawerComponent/>
                </Header>
            </Layout>
        </>
    );
}

export default Nav;