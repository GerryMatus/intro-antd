import React from 'react';
import { useAuth } from '../../hooks/useAuth'
import { Button } from 'antd'
import Nav from '../../components/Nav/'

const Home = () => {
    const { user, logout } = useAuth();
    return (
    <>
        <Nav/>
        <h1>Hola {user.username}</h1>
        <Button onClick={() => logout()}>Cerrar sesión</Button>
    </>
    );
}

export default Home;