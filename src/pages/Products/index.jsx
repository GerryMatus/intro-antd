import React, { useEffect, useState } from 'react';
import Nav from '../../components/Nav';
import { Divider, Table } from 'antd';
import { getProducts } from '../../services/products';
import CrudButton from '../../components/CrudBotton';

const columns = [
    {
        title: 'Nombre',
        dataIndex: 'name',
    },
    {
        title: 'Precio',
        dataIndex: 'price',
    },
    {
        title: 'Categoria',
        dataIndex: 'category',
    },
    {
        title: 'Imagen',
        dataIndex: 'imgURL',
        render: (url) => <img src={url} alt='product' style={{ width: 100 }} />
    }
];

const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, `selectedRows: ${selectedRows}`);
    },
    getCheckboxProps: (record) => ({
        disabled: record.name === 'Disabled User',
        name: record.name,
    }),
}


const Products = () => {
    const [products, setProducts] = useState([]);
    const [selectionType, setSelectionType] = useState("checkbox");

    useEffect(() => {
        const fetchProducts = async () => {
            try {
                const data = await getProducts();
                const productsWithKey = data.map(product => ({
                    ...product,
                    key: product._id,
                }));
                setProducts(productsWithKey);
            } catch (error) {
                console.error('Error al obtener productos:', error);
            }
        };

        fetchProducts();
    }, []);

    return (
        <div>
            <Nav />
            <br/>
            <CrudButton/>
            <br/>
            <div className='products-container'>
                <Table
                    rowSelection={{
                        type: selectionType,
                        ...rowSelection,
                    }}
                    columns={columns}
                    dataSource={products}
                    scroll={{ y: 400 }}
                />
            </div>
        </div>
    );
}

export default Products;