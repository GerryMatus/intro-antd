import React from 'react';
import LayoutComponent from '../../components/Layout';
import FormRegister from '../../components/Register/'
import Image from '../../components/FormLogin/Image.jsx'

const Register = () => {
    return (
        <LayoutComponent
            leftColSize={{ xs: 12, sm: 12, md: 40, lg: 36 }}
            rigthColSize={{ xs: 48, sm: 48, md: 8, lg: 12 }}
            leftContent={<Image />}
            rightContent={<FormRegister />}
        />
    );
}

export default Register;