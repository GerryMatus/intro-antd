import React from 'react';
import { ConfigProvider } from 'antd';
import './App.css';
import { BrowserRouter } from 'react-router-dom';
import AppRoutes from './routes';
import { AuthProvider } from './context/AuthContext';

function App() {
  
  return (
    <AuthProvider>
      <ConfigProvider 
      theme={{
        token:{
          colorPrimary:'#0000FF'
        }
      }}
      >

        <BrowserRouter> 
          <AppRoutes /> 
        </BrowserRouter>
      </ConfigProvider>
    </AuthProvider>
      
     
    
  )
}

export default App